/*
* UART.h
*
* Created: 2017-06-15 10:29:19
*  Author: Reuberik
*/


#ifndef UART_H_
#define UART_H_

#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>
#include "pins.h"

void init_UART(void);
void uart_write(char);
uint8_t uart_char_is_waiting(void);
char uart_read(void);

#endif /* UART_H_ */