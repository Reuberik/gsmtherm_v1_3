/*
* interrupts.c
*
* Created: 2017-06-15 11:21:59
*  Author: Reuberik
*/


#include "interrupts.h"



void init_timer1(void)
{
    TCCR1B = 0x0C;
    OCR1A = 62500;
    TIMSK1 = 0x02;
    asm("SEI");
}




void wait_busyflag(void)
{
    char byte, dummy;
    DDRF &= 0x00;		//PF4-PF7 inputs
    PORTD |= (1<<RW);	//RW = 1
    PORTD &= ~(1<<RS);	//RS = 0
    do
    {
        PORTC |= (1<<E);	//show contents of LCD
        asm("NOP");
        asm("NOP");
        byte = PINF & 0x80;	//MSB of byte is busyflag
        PORTC &= ~(1<<E);	//disconnect LCD
        PORTC |= (1<<E);	//show contents of LCD
        asm("NOP");
        asm("NOP");
        dummy = PINF;	//MSB of byte is busyflag
        PORTC &= ~(1<<E);	//disconnect LCD
    }
    while(byte);
    PORTD &= ~(1<<RW);	//read from LCD
    DDRF = 0xF0;		//PF4-PF7 outputs
}
