/*
* interrupts.h
*
* Created: 2017-06-15 11:23:25
*  Author: Reuberik
*/


#ifndef TIMERS_H_
#define TIMERS_H_

#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>
#include "pins.h"

void wait_busyflag(void);
void init_timer1(void);

int AD_result;

#endif /* TIMERS_H_ */