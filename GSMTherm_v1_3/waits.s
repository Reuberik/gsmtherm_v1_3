
/*
 * waits.s
 *
 * Created: 2017-06-07 11:34:25
 *  Author: Reuberik
 */ 
 //waits.s

#include <avr/io.h>
.global wait
.global wait_long
.global no_operation

wait:	NOP
		DEC	R24
		BRNE	wait
		RET

wait100:	LDI	R16, 199	//199 f�r att kompensera f�r CALL och RET
decrement:	NOP
		NOP
		NOP
		NOP
		NOP
		DEC	R16
		BRNE	decrement
		RET

wait_long:	CALL	wait100	//denna raden inkl CALL tar 100 �s
		DEC	R24
		BRNE	wait_long
		RET

no_operation:	RET