/*
* inits.c
*
* Created: 2017-06-15 11:34:59
*  Author: Reuberik
*/

#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>
#include "UART.h"
#include "pins.h"
#include "interrupts.h"








void init_LCD(void)
{
    PORTD &= ~(1 << RS);	//RS=0
    send_nibble(0x30);
    wait_long(41); //wait 4,1 ms
    send_nibble(0x30);
    wait_long(1); //wait 100 us
    send_nibble(0x30);
    send_nibble(0x20);
    wait(170);	// wait (4*170+ 8)/16 ?s = 43 us
    send_instruction(0x28);
    send_instruction(0x0F);
    send_instruction(0x01);
    wait_long(16);
    send_instruction(0x06);
}

void send_nibble(char halfbyte)
{
    PORTF = halfbyte;
    PORTC |= (1 << E);//start pulse
    wait(1);	//pulse length 750 ns
    PORTC &=!(1 << E);  //end pulse
}

void send_instruction(char byte)
{
    wait_busyflag();
    PORTD &= ~(1 << RS);
    PORTD &= ~(1 << RW);
    send_nibble(byte);
    send_nibble(byte<<4);
}

void send_character(char byte)
{
    wait_busyflag();
    PORTD |= (1 << RS);	//RS = 1, character
    PORTD &= ~(1 << RW);	//RW =0
    send_nibble(byte);
    send_nibble(byte<<4);
}

void send_decimal(char num)
{
    if(num / 100 != 0)
    send_character(num / 100 + 0x30);
    num %= 100;
    if(num / 10 != 0)
    send_character(num / 10 + 0x30);
    send_character(num % 10 + 0x30);
}

void send_string(char * str)
{
    char a = *str;
    for(int i = strlen(str); i > 0; i--)
    {
        send_character(a);
        a = *(++str);
    }
}

void send_temp(unsigned int degrees)
{
    send_instruction(0xC0);
    if(degrees / 100 + 0x30 != 0)
    send_character(degrees / 100 + 0x30);
    degrees %= 100;
    if(degrees / 10 + 0x30 != 0)
    send_character(degrees / 10 + 0x30);
    send_character(',');
    send_character(degrees % 10 + 0x30);
    send_character(0xDF);
    send_character('C');
    send_instruction(0xC7);
    

}

void init_AD(void)
{
    ADMUX |= 0x49;
    ADCSRA |= (1 << 5);
    ADCSRA |= 0x07;
    
    ADCSRA |= (1 << 7);
    ADCSRA |= (1 << 6);
    DIDR0 |= 0x02;
}
