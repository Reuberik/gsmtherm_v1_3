/*
* pins.h
*
* Created: 2017-06-15 11:31:41
*  Author: Reuberik
*/


#ifndef PINS_H_
#define PINS_H_

#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>

void init_pins(void);

#define 	RS 	0
#define 	RW 	1
#define 	E 	7

#endif /* PINS_H_ */