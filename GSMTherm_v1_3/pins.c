/*
 * pins.c
 *
 * Created: 2017-06-15 14:18:00
 *  Author: Reuberik
 */ 

 #include "pins.h"

 void init_pins(void)
 {
     DDRD = 0xFF;	//PD4-PD7 outputs
     PORTD = 0xFF;
     DDRF = 0xF0;	//PF4-PF7 outputs
     DDRC = 0xF0;	//PC6 and PC7 outputs
     DDRB = 0x0F;	//PB4-PB7 inputs
     PORTB = 0xF0;	//connect pullup resistors
     PORTC = 0x0F;
 }