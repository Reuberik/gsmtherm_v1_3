/*
* inits.h
*
* Created: 2017-06-15 11:35:14
*  Author: Reuberik
*/


#ifndef ADC_H_
#define ADC_H_


void init_LCD(void);
void init_AD(void);
void send_nibble(char);
void send_instruction(char);
void send_character(char);
void send_decimal(char);
void send_string(char *);
void send_temp(unsigned int);


#endif /* ADC_H_ */