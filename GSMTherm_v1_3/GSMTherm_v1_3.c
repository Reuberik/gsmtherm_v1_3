/*
* GSMTherm_v1_3.c
*
* Created: 2017-06-07 11:29:23
* Author : Reuberik
*/

/* Std headers */
#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>

/* Custom headers */
#include "UART.h"
#include "interrupts.h"
#include "LCD.h"
#include "converters.h"

/* Extern assembly-functions */
extern void wait(char);
extern void wait_long(char);
extern void no_operation(void);
int count = 0;
char sent = 0;
char num[1024] = {0};



int main(void)
{
    
    SPL = 0xFF;
    SPH = 0x0A;
    USBCON = 0x00;
    init_pins();
    init_LCD();
    init_AD();
    init_timer1();
    init_UART();
    
    char str_snd[1024] = {0};
    char str[1024] = {0};
    char read = 0;
    int i = 0;

    strcat(str_snd, "AT\r\n");

    for (int i = 0; i < strlen(str_snd); i++)
    {
        uart_write(str_snd[i]);
    }
    wait_long(255);
    strcat(str_snd, "AT+IPR=\"19200\";\r\n");

    for (int i = 0; i < strlen(str_snd); i++)
    {
        uart_write(str_snd[i]);
    }
    
    wait_long(255);
    str_snd[0] = 0;
    strcat(str_snd, "AT+CNMI 1,2,0,0,0;\r\n");

    for (int i = 0; i < strlen(str_snd); i++)
    {
        uart_write(str_snd[i]);
    }
    
    wait_long(255);
    for (int x = 0; x < strlen(str_snd); x++)
    {
        str_snd[0] = 0;
    }

    while(1)
    {
        //read = uart_read();
        //strcat(str, read);
        //if(strlen(num) > 0)
        //{
            //send_string(num);
            //for(int i = 0; i < strlen(num); i++)
                //num[i] = 0;
        //}
        //if(strlen(str) > 50)
        //{
            //if(strncmp(str, "+46", 3) || strncmp(str, "073", 3))
            //{
                //int a = 1;
                //for(int x = 0; x < strlen(str); x++)
                //{
                    //if(str[x] == '+' || str[x] == '0')
                    //{
                        //num[0] = '\n';
                    //}
                    //if(num[0] == '\n')
                    //{
                        //while(str[x] != '\"')
                        //{
                            //num[a] = str[x];
                            //a++;
                            //x++;
                        //}
                        //num[0] = '\"';
                        //num[a+1] = '\"';
                    //}
                //}
                //
                //a = 1;
                //for(int q = 0; q < strlen(str); q++)
                    //str[q] = 0;
            //}
        //}
    }

}

ISR(TIMER1_COMPA_vect)
{
    int i = 0;
    char temp[5] = {0};
    char str[1024] = {0};
    char read = 0;
    char str_snd[1024] = {0};
    AD_result = ADCL;
    AD_result += ((ADCH-1) * 256);
    
    AD_result *= 2;
    AD_result = (float)AD_result * (float)0.9;
    


    if(AD_result > 310)
    {

        

        //str[0] = 0;
        strcat(str_snd, "AT+CMGR=1\r\n");
        
        for (int i = 0; i < strlen(str_snd); i++)
        {
            uart_write(str_snd[i]);
        }
        wait_long(255);


        if (!sent)
        {
            
            if(count > 5)
            {
                itoa(AD_result, temp);
                strcat(str_snd, "AT+CFUN=1\r\n");

                for (int i = 0; i < strlen(str_snd); i++)
                {
                    uart_write(str_snd[i]);
                }

                wait_long(100);
                str_snd[0] = '\0';
                strcat(str_snd, "AT+CREG?\r\n");

                for (int i = 0; i < strlen(str_snd); i++)
                {
                    uart_write(str_snd[i]);
                }
                
                wait_long(100);

                str_snd[0] = '\0';
                strcat(str_snd, "\r\nAT+CMGF=1;\r\n");

                for (int i = 0; i < strlen(str_snd); i++)
                {
                    uart_write(str_snd[i]);
                }
                
                wait_long(100);
                str_snd[0] = '\0';
                strcat(str_snd, "\r\nAT+CMGS=\"+46739537797\"\r\n");

                for (int i = 0; i < strlen(str_snd); i++)
                {
                    uart_write(str_snd[i]);
                }
                //
                //for (int i = 0; i < strlen(num); i++)
                //{
                //uart_write(num[i]);
                //}
                //strcat(str_snd, "\r\n");
                //
                //for (int i = 0; i < strlen(str_snd); i++)
                //{
                //uart_write(str_snd[i]);
                //}
                wait_long(100);
                
                str_snd[0] = '\0';
                strcat(str_snd, "Temperaturen �r nu �ver ");
                
                temp[strlen(temp) - 1] = '\0';
                strcat(str_snd, temp);
                strcat(str_snd, " grader Celsius.");

                for (int i = 0; i < strlen(str_snd); i++)
                {
                    uart_write(str_snd[i]);
                }
                
                wait_long(100);

                wait_long(100);
                str_snd[0] = '\0';
                str_snd[1] = '\0';
                str_snd[0] = (char)26;
                for (int i = 0; i < strlen(str_snd); i++)
                {
                    uart_write(str_snd[i]);
                }


                i = 0;
                
                wait_long(100);
                sent++;
                count = 0;
                
                
            }
            count++;
        }

    }
    send_temp(AD_result);
    if(AD_result < 310)
    {
        sent = 0;
        count = 0;
    }


}




