/*
* UART.c
*
* Created: 2017-06-15 10:17:02
*  Author: Reuberik
*/

#include "UART.h"

void init_UART()
{
    // set baud rate
    UBRR1H = 0;
    UBRR1L = 51;  
    UCSR1B = (1<<RXEN1)|(1<<TXEN1);
    // set 8N1 frame format
    UCSR1C = (1<<UCSZ11)|(1<<UCSZ10);
}

void uart_write(char x) {
    // wait for empty receive buffer
    while ((UCSR1A & (1<<UDRE1))==0);
    // send
    UDR1 = x;
}

uint8_t uart_char_is_waiting(void) {
    // returns 1 if a character is waiting
    // returns 0 if not
    return (UCSR1A & (1<<RXC1));
}

char uart_read(void) {
    // wait
    while(!uart_char_is_waiting());
    char x = UDR1;
    return x;
}

